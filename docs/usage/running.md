# Running

There are 3 options for running this application:

Option 1: Build a docker image and use it  
Option 2: Install Node.JS (see below for detailed usage)
Option 3: Use a prebuilt docker image  

After starting it, you can open ``http://server/time`` to test it,
it should show a timestamp


